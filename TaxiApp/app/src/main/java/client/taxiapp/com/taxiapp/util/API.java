package client.taxiapp.com.taxiapp.util;

/**
 * Created by Amence on 2016/11/3.
 */

public class API {

    //serverUrl
//    public static String URL = "http://192.168.2.175:8080/";
    public static String URL = "http://116.196.115.0:8080/";

    //注册
    public static String REGISTER           = URL+"register";

    //登录
    public static String LOGIN              = URL+"login";

    //找回密码
    public static String BACK               = URL+"change";

    //获取所有司机
    public static String ALL_DRIVER          = URL+"getAllDriver";

    //获取所有乘客
    public static String ALL_PASSENGER       = URL+"getAllPassenger";


    //司机或者乘客完成行程
    public static String FINISH              = URL+"finish";



}
