package client.taxiapp.com.taxiapp.util;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by Amence on 2018/4/29.
 */

public class RetrofitUtil {

    public static Retrofit retrofitRequest(){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }



}
