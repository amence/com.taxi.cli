package client.taxiapp.com.taxiapp.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Amence on 2018/4/28.
 */

public class User implements Parcelable {

    int            id;          //id
    int            userType;    //1:表示乘客 2：表示司机
    String         userName;    //昵称
    String         password;    //密码
    String         email;       //邮箱
    int            userWork;    //1:表示空闲  2：表示乘客司机都在工作
    double         latitude;    //纬度
    double         longitude;   //经度

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getUserWork() {
        return userWork;
    }

    public void setUserWork(Integer userWork) {
        this.userWork = userWork;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public User() {
    }

    public User(String name, String pwd, String email){
        this.userName = name;
        this.password = pwd;
        this.email = email;

    }
    public User(String name,String pwd){
        this.userName = name;
        this.password = pwd;

    }


    protected User(Parcel in) {
        id = in.readInt();
        userType = in.readInt();
        userName = in.readString();
        password = in.readString();
        email = in.readString();
        userWork = in.readInt();
        latitude = in.readDouble();
        longitude = in.readDouble();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(userType);
        dest.writeString(userName);
        dest.writeString(password);
        dest.writeString(email);
        dest.writeInt(userWork);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
    }
}
