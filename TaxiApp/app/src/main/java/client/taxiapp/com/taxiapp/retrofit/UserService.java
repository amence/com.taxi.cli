package client.taxiapp.com.taxiapp.retrofit;

import client.taxiapp.com.taxiapp.domain.BaseResponse;
import client.taxiapp.com.taxiapp.domain.User;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;


/**
 * Created by Amence on 2018/4/28.
 */

public interface UserService {


    //登录
    @Headers({"Content-Type: application/json","Accept: application/json"})//需要添加头
    @POST("/taxi/login")
    Call<BaseResponse> login(@Body User user);

    //注册
    @Headers({"Content-Type: application/json","Accept: application/json"})//需要添加头
    @POST("/taxi/register")
    Call<BaseResponse> register(@Body User user);

    //找回
    @Headers({"Content-Type: application/json","Accept: application/json"})//需要添加头
    @POST("/taxi/change")
    Call<BaseResponse> returnBack(@Body User user);


    //获取司机
    @Headers({"Content-Type: application/json","Accept: application/json"})//需要添加头
    @GET("/taxi/getAllDriver")
    Call<BaseResponse> getAllDriver();


    //获取乘客
    @Headers({"Content-Type: application/json","Accept: application/json"})//需要添加头
    @GET("/taxi/getAllPassenger")
    Call<BaseResponse> getAllPassenger();


    //司机完成   乘客完成
    @Headers({"Content-Type: application/json","Accept: application/json"})//需要添加头
    @GET("/taxi/finish")
    Call<BaseResponse> finish(@Body User user);



    @Headers({"Content-Type: application/json","Accept: application/json"})//需要添加头
    @POST("/taxi/finish")
    Call<BaseResponse> updateLocation(@Body User user);

    //乘客上车 司机接客完成
    @Headers({"Content-Type: application/json","Accept: application/json"})//需要添加头
    @GET("/taxi/working")
    Call<BaseResponse> working(@Body User user);


    //司机乘客开始接口 开始叫车
    @Headers({"Content-Type: application/json","Accept: application/json"})//需要添加头
    @GET("/taxi/waiting")
    Call<BaseResponse> waiting(@Body User user);


    //司机乘客开始接口 开始叫车
    @Headers({"Content-Type: application/json","Accept: application/json"})//需要添加头
    @POST("/taxi/getUser")
    Call<BaseResponse> getUser(@Body User user);




}
