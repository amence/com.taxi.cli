package client.taxiapp.com.taxiapp.activity;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.InfoWindow;
import com.baidu.mapapi.map.MapPoi;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.MyLocationConfiguration;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.route.BikingRouteResult;
import com.baidu.mapapi.search.route.DrivingRoutePlanOption;
import com.baidu.mapapi.search.route.DrivingRouteResult;
import com.baidu.mapapi.search.route.IndoorRouteResult;
import com.baidu.mapapi.search.route.MassTransitRouteResult;
import com.baidu.mapapi.search.route.OnGetRoutePlanResultListener;
import com.baidu.mapapi.search.route.PlanNode;
import com.baidu.mapapi.search.route.RoutePlanSearch;
import com.baidu.mapapi.search.route.TransitRouteResult;
import com.baidu.mapapi.search.route.WalkingRouteResult;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import client.taxiapp.com.taxiapp.R;
import client.taxiapp.com.taxiapp.customer.SubmitButton;
import client.taxiapp.com.taxiapp.customer.util.Utils;
import client.taxiapp.com.taxiapp.domain.BaseResponse;
import client.taxiapp.com.taxiapp.domain.User;
import client.taxiapp.com.taxiapp.retrofit.UserService;
import client.taxiapp.com.taxiapp.service.PassengerServer;
import client.taxiapp.com.taxiapp.util.Constant;
import client.taxiapp.com.taxiapp.util.RetrofitUtil;
import client.taxiapp.com.taxiapp.util.Util;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class MainActivity extends Activity implements BaiduMap.OnMarkerClickListener
        , BaiduMap.OnMapClickListener, BaiduMap.OnMapStatusChangeListener {

    private MapView mMapView = null;

    private BaiduMap mBaiduMap;

    ScheduledExecutorService scheduleForUserPull = null;

    private BaiduMap.OnMarkerClickListener markerListener;

    private Button callBut;

    private Button finishBut;

    private User user;

    private ImageView updateLocationImg;

    FloatingActionButton dh;
    FloatingActionButton mn;

    //显示的动画
    private TranslateAnimation shwoAction;
    //隐藏时的动画
    private TranslateAnimation hideAction;

    //infowindow
    private BitmapDescriptor mBitmapWindow;

    private LatLng currentLatLng = null;
    private InfoWindow infoWindow;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String userJson = Util.getString(this, Constant.SP_USER_KEY);

        user = JSON.parseObject(userJson, User.class);

        initView();

        initLayout();

        initData(user);

        initListener();

    }

    private void initView() {
        shwoAction = new TranslateAnimation(Animation.RELATIVE_TO_SELF, -1.0f,
                Animation.RELATIVE_TO_SELF, 0f, Animation.RELATIVE_TO_SELF,
                0.0f, Animation.RELATIVE_TO_SELF, 0.0f);
        shwoAction.setDuration(300);

        hideAction = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF,
                0.0f, Animation.RELATIVE_TO_SELF, 0.0f);
        hideAction.setDuration(300);
    }

    private void stopAnimation() {
        if (dh.getVisibility() == View.GONE || mn.getVisibility() == View.GONE)
            return;
        dh.startAnimation(hideAction);
        mn.startAnimation(hideAction);
        dh.setVisibility(View.GONE);
        mn.setVisibility(View.GONE);
    }

    private void startAnimation() {
        if (dh.getVisibility() == View.VISIBLE || mn.getVisibility() == View.VISIBLE)
            return;
        dh.startAnimation(shwoAction);
        mn.startAnimation(shwoAction);
        dh.setVisibility(View.VISIBLE);
        mn.setVisibility(View.VISIBLE);
    }

    private void initLayout() {
        mMapView = (MapView) findViewById(R.id.bmapView);
        callBut = (Button) findViewById(R.id.callBut);
        finishBut = (Button) findViewById(R.id.finish);
        dh = (FloatingActionButton) findViewById(R.id.dh);
        mn = (FloatingActionButton) findViewById(R.id.mn);
        updateLocationImg = (ImageView) findViewById(R.id.updateLocation);
        mBaiduMap = mMapView.getMap();
        //开启定位图层
        mBaiduMap.setMyLocationEnabled(true);


    }

    private void initListener() {

        updateLocationImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initData(user);
            }
        });

        callBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMapView.showZoomControls(true);
                mMapView.showScaleControl(true);
                addMaker(user);
                callBut.setVisibility(View.INVISIBLE);
                finishBut.setVisibility(View.VISIBLE);
            }
        });


    }

    /**
     * 定位用户位置
     *
     * @param user
     */
    private void initData(User user) {

//        Location location = Utils.getLocation(this);

        // 构造定位数据
        MyLocationData locData = new MyLocationData.Builder()
                .accuracy(0)
                // 此处设置开发者获取到的方向信息，顺时针0-360
                .direction(0).latitude(0)
                .longitude(0).build();


        //覆盖物的点击事件
        mBaiduMap.setOnMarkerClickListener(this);
        //地图的点击事件
        mBaiduMap.setOnMapClickListener(this);
        //地图状态事件
        mBaiduMap.setOnMapStatusChangeListener(this);

        // 设置定位数据
        mBaiduMap.setMyLocationData(locData);

        // 设置定位图层的配置（定位模式，是否允许方向信息，用户自定义定位图标）
        BitmapDescriptor mCurrentMarker = BitmapDescriptorFactory.fromResource(R.mipmap.navi_map_gps_locked);

        MyLocationConfiguration config = new MyLocationConfiguration(MyLocationConfiguration.LocationMode.COMPASS, true, mCurrentMarker);
        mBaiduMap.setMyLocationConfigeration(config);

        //开启比例尺
        mMapView.showZoomControls(true);
        mMapView.showScaleControl(true);
        MapStatusUpdate status = MapStatusUpdateFactory.zoomTo(15.0F);
        mBaiduMap.setMapStatus(status);


    }


    private void addMaker(User user) {

        if (user.getUserType() == 1) {//如果是乘客就获取司机
            scheduleForUserPull = Executors.newSingleThreadScheduledExecutor();
            final BitmapDescriptor bitmap = BitmapDescriptorFactory
                    .fromResource(R.drawable.car_64);
            scheduleForUserPull.scheduleWithFixedDelay(new Runnable() {
                public void run() {
                    try {
                        String driversJson = Util.getString(MainActivity.this, Constant.SP_DRIVER_KEY);
                        List<User> customers = JSON.parseArray(driversJson, User.class);

                        buildMarker(customers, bitmap);
                    } catch (Throwable e) {
                        Log.v("Amence", "scheduleForUserPull error" + e.getMessage());
                    }

                }
            }, Constant.DEFAULT_SCHEDULE_INITIAL_DELAY, Constant.DEFAULT_SCHEDULE_LOCATION_DELAY, TimeUnit.SECONDS);


        } else {
            final BitmapDescriptor bitmap = BitmapDescriptorFactory
                    .fromResource(R.drawable.passenger_orange);
            scheduleForUserPull = Executors.newSingleThreadScheduledExecutor();
            scheduleForUserPull.scheduleWithFixedDelay(new Runnable() {
                public void run() {
                    try {
                        String driversJson = Util.getString(MainActivity.this, Constant.SP_PASSENGER_KEY);
                        List<User> customers = JSON.parseArray(driversJson, User.class);
                        buildMarker(customers, bitmap);
                    } catch (Throwable e) {
                        Log.v("Amence", "scheduleForUserPull error" + e.getMessage());
                    }

                }
            }, Constant.DEFAULT_SCHEDULE_INITIAL_DELAY, Constant.DEFAULT_SCHEDULE_LOCATION_DELAY, TimeUnit.SECONDS);
        }


    }

    /**
     * 标记marker
     *
     * @param customers
     */
    private void buildMarker(List<User> customers, BitmapDescriptor bitmap) {

        if (customers != null && customers.size() > 0) {
            Marker marker = null;
            for (int i = 0; i < customers.size(); i++) {
                if (customers.get(i).getLongitude() != null && customers.get(i).getLatitude() != null) {
                    LatLng point = new LatLng(customers.get(i).getLatitude(), customers.get(i).getLongitude());
                    OverlayOptions option = new MarkerOptions().position(point).icon(bitmap).zIndex(5);

                    marker = (Marker) mBaiduMap.addOverlay(option);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("user",customers.get(i));
                    marker.setExtraInfo(bundle);
                }

            }

        }

        //在地图上批量添加
//        mBaiduMap.addOverlays(options);

//        //地图 Marker 覆盖物点击事件监听接口：
//        mBaiduMap.setOnMarkerClickListener(markerListener);

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        //在activity执行onDestroy时执行mMapView.onDestroy()，实现地图生命周期管理
        mMapView.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //在activity执行onResume时执行mMapView. onResume ()，实现地图生命周期管理
        mMapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //在activity执行onPause时执行mMapView. onPause ()，实现地图生命周期管理
        mMapView.onPause();
    }

    @Override
    public void onMapClick(LatLng latLng) {
        mBaiduMap.hideInfoWindow();
        stopAnimation();
    }


    @Override
    public boolean onMapPoiClick(MapPoi mapPoi) {
        return false;
    }

    @Override
    public void onMapStatusChangeStart(MapStatus mapStatus) {

    }

    @Override
    public void onMapStatusChangeStart(MapStatus mapStatus, int i) {

    }


    @Override
    public void onMapStatusChange(MapStatus mapStatus) {
        mBaiduMap.hideInfoWindow();
        stopAnimation();
    }

    @Override
    public void onMapStatusChangeFinish(MapStatus mapStatus) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        Bundle bundle = marker.getExtraInfo();
        User user = bundle.getParcelable("user");
        TextView infoText = new TextView(getApplicationContext());
        infoText.setBackgroundResource(R.mipmap.location_tips);
        infoText.setPadding(30, 20, 30, 30);
        infoText.setText(user.getUserName());
        infoText.setTextColor(Color.parseColor("#FFFFFF"));

        mBitmapWindow = BitmapDescriptorFactory.fromView(infoText);
        final LatLng lng = marker.getPosition();
        Point p = mBaiduMap.getProjection().toScreenLocation(lng);
        p.y -= 47;
        currentLatLng = mBaiduMap.getProjection().fromScreenLocation(p);

        infoWindow = new InfoWindow(mBitmapWindow, currentLatLng, 0, new InfoWindow.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick() {
                mBaiduMap.hideInfoWindow();
                stopAnimation();
            }
        });
        mBaiduMap.showInfoWindow(infoWindow);
        startAnimation();
        return true;
    }
}
