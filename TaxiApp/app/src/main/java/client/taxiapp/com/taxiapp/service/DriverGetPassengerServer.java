package client.taxiapp.com.taxiapp.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.UiThread;
import android.util.Log;

import com.alibaba.fastjson.JSON;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import client.taxiapp.com.taxiapp.domain.BaseResponse;
import client.taxiapp.com.taxiapp.domain.PassengerDriverLink;
import client.taxiapp.com.taxiapp.domain.User;
import client.taxiapp.com.taxiapp.retrofit.LinkService;
import client.taxiapp.com.taxiapp.retrofit.UserService;
import client.taxiapp.com.taxiapp.util.Constant;
import client.taxiapp.com.taxiapp.util.RetrofitUtil;
import client.taxiapp.com.taxiapp.util.Util;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class DriverGetPassengerServer extends Service {


    ScheduledExecutorService scheduledForDriverGetPassenger = null;

    public DriverGetPassengerServer() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }


    @Override
    public void onCreate() {
        super.onCreate();


        scheduledForDriverGetPassenger = Executors.newSingleThreadScheduledExecutor();
        scheduledForDriverGetPassenger.scheduleWithFixedDelay(new Runnable() {
            public void run() {
                try {
                    Retrofit retrofit = RetrofitUtil.retrofitRequest();

                    LinkService linkService = retrofit.create(LinkService.class);
                    String userJson = Util.getString(DriverGetPassengerServer.this, Constant.SP_USER_KEY);
                    User user = JSON.parseObject(userJson, User.class);
                    PassengerDriverLink link = new PassengerDriverLink();
                    link.setDriverName(user.getUserName());
                    Call<BaseResponse> call = linkService.getPassenger(link);

                    call.enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Response<BaseResponse> response, Retrofit retrofit) {

                            BaseResponse baseResponse = response.body();

                            if (baseResponse != null && baseResponse.getMeta().isResult()) {
                                String linkPassenger = JSON.toJSONString(baseResponse.getData());
                                Log.v("Amence", linkPassenger);
                                Util.saveString(DriverGetPassengerServer.this, Constant.SP_DRIVER_GET_PASSENGER_KEY, linkPassenger);
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            String message = t.getMessage();

                        }

                    });

                } catch (Throwable e) {
                    Log.v("Amence", "scheduledForDriverPull error" + e.getMessage());
                }

            }
        }, Constant.DEFAULT_SCHEDULE_INITIAL_DELAY, 20, TimeUnit.SECONDS);

        Log.v("Amence", "DriverServer-->onCreate()");
    }
}
