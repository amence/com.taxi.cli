package client.taxiapp.com.taxiapp.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ZoomControls;

import com.alibaba.fastjson.JSON;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.InfoWindow;
import com.baidu.mapapi.map.MapPoi;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.MyLocationConfiguration;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.core.RouteLine;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.route.BikingRouteResult;
import com.baidu.mapapi.search.route.DrivingRoutePlanOption;
import com.baidu.mapapi.search.route.DrivingRouteResult;
import com.baidu.mapapi.search.route.IndoorRouteResult;
import com.baidu.mapapi.search.route.MassTransitRouteResult;
import com.baidu.mapapi.search.route.OnGetRoutePlanResultListener;
import com.baidu.mapapi.search.route.PlanNode;
import com.baidu.mapapi.search.route.RoutePlanSearch;
import com.baidu.mapapi.search.route.TransitRouteResult;
import com.baidu.mapapi.search.route.WalkingRouteResult;
import com.baidu.navisdk.adapter.BNCommonSettingParam;
import com.baidu.navisdk.adapter.BNRoutePlanNode;
import com.baidu.navisdk.adapter.BNaviSettingManager;
import com.baidu.navisdk.adapter.BaiduNaviManager;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import at.markushi.ui.CircleButton;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import client.taxiapp.com.taxiapp.R;
import client.taxiapp.com.taxiapp.customer.ZoomControView;
import client.taxiapp.com.taxiapp.domain.BaseResponse;
import client.taxiapp.com.taxiapp.domain.PassengerDriverLink;
import client.taxiapp.com.taxiapp.domain.User;
import client.taxiapp.com.taxiapp.map.DrivingRouteOverlay;
import client.taxiapp.com.taxiapp.map.MyOrentationListener;
import client.taxiapp.com.taxiapp.map.OverlayManager;
import client.taxiapp.com.taxiapp.retrofit.LinkService;
import client.taxiapp.com.taxiapp.retrofit.UserService;
import client.taxiapp.com.taxiapp.util.Constant;
import client.taxiapp.com.taxiapp.util.RetrofitUtil;
import client.taxiapp.com.taxiapp.util.Util;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class Main2Activity extends AppCompatActivity implements BaiduMap.OnMarkerClickListener, BaiduMap.OnMapClickListener, BaiduMap.OnMapStatusChangeListener,
        OnGetRoutePlanResultListener {

    @BindView(R.id.mMapView)
    MapView mMapView;

    private static final String TAG = "Main2Activity";
    @BindView(R.id.dh)
    FloatingActionButton dh;
    @BindView(R.id.mn)
    FloatingActionButton mn;
    @BindView(R.id.zoom)
    ZoomControView zoom;
    private BaiduMap mBaiduMap;
    private LocationClient mLocationClient;
    private MyLoacationListener myLocationListener;
    private boolean isFirstIn = true;

    private double mLatitude;
    private double mLongitude;
    private double mEndLatitude;
    private double mEndLongitude;
    private BitmapDescriptor mLoacationBitmap;

    private MyOrentationListener myOrentationListener;
    private float mCurrentX;//当前位置

    private MyLocationConfiguration.LocationMode mLocationMode;
    int nodeIndex = -1; // 节点索引,供浏览节点时使用

    //覆盖物
    private BitmapDescriptor mMarker;

    //infowindow
    private BitmapDescriptor mBitmapWindow;
    private InfoWindow infoWindow;

    //显示的动画
    private TranslateAnimation shwoAction;
    //隐藏时的动画
    private TranslateAnimation hideAction;

    //当前选择的经纬度
    private LatLng currentLatLng = null;
    private LatLng mEndLatLng = null;
    DrivingRouteResult nowResultdrive = null;


    //导航相关权限
    private static final String[] authBaseArr = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION};
    private static final String[] authComArr = {Manifest.permission.READ_PHONE_STATE};
    private static final int authBaseRequestCode = 1;
    private static final int authComRequestCode = 2;
    private String APP_FOLDER_NAME = "lib";
    private String mSDCardPath = null;
    public static final String ROUTE_PLAN_NODE = "routePlanNode";
    public static List<Activity> activityList = new LinkedList<Activity>();
    private boolean hasInitSuccess = false;//百度导航是否初始化成功
    private boolean hasRequestComAuth = false;
    RoutePlanSearch mSearch = null;
    boolean hasShownDialogue = true;
    RouteLine route = null;

    OverlayManager routeOverlay = null;

    User aimDriver;

    ScheduledExecutorService scheduleForUserPull = null;

    ScheduledExecutorService scheduleForShowRoad = null;

    ScheduledExecutorService scheduleForPassengerPull = null;

    ScheduledExecutorService scheduleForGetDriver = null;

    private User user;

    @BindView(R.id.callDriverBut)
    Button callDriverBut;

    @BindView(R.id.makeSureBut)
    Button makeSureBut;

    @BindView(R.id.finishBut)
    Button finishBut;

    @BindView(R.id.showRoad)
    Button showRoadBut;

    @BindView(R.id.payLinearLayout)
    LinearLayout payLinearLayout;

    @BindView(R.id.weChatPayBut)
    CircleButton weChatPayBut;

    @BindView(R.id.aliPayBut)
    CircleButton aliPayBut;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main2);
        ButterKnife.bind(this);
        String userJson = Util.getString(this, Constant.SP_USER_KEY);

        user = JSON.parseObject(userJson, User.class);

        ButterKnife.bind(this);
        initView();
        initMarker(user);
        initLocation();


        if (initDirs()) {
            initNavigatin();
        }

    }

    String authinfo = null;

    /***************************************
     * 导航部分
     ***************************************************/
    private void initNavigatin() {
        //申请权限
        if (Build.VERSION.SDK_INT >= 23) {
            if (!hasBasePhoneAuth()) {
                this.requestPermissions(authBaseArr, authBaseRequestCode);
                return;
            }
        }
        //初始化导航
        BaiduNaviManager.getInstance().init(this, mSDCardPath, APP_FOLDER_NAME, new BaiduNaviManager.NaviInitListener() {
            @Override
            public void onAuthResult(int status, String msg) {
                if (0 == status) {
                    authinfo = "key校验成功!";
                } else {
                    authinfo = "key校验失败, " + msg;
                }
                Main2Activity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(Main2Activity.this, authinfo, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void initStart() {
                Log.v("Amence", "百度导航初始化开始");

                Toast.makeText(Main2Activity.this, "百度导航初始化开始", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void initSuccess() {
                Log.v("Amence", "百度导航初始化成功");
                Toast.makeText(Main2Activity.this, "百度导航初始化成功", Toast.LENGTH_SHORT).show();
                hasInitSuccess = true;
                initSetting();
            }

            @Override
            public void initFailed() {
                Log.v("Amence", "百度导航引擎初始化失败");

                Toast.makeText(Main2Activity.this, "百度导航引擎初始化失败", Toast.LENGTH_SHORT).show();
            }
        }, null, ttsHandler, ttsPlayStateListener);
    }


    /**
     * 内部TTS播报状态回传handler
     */
    private Handler ttsHandler = new Handler() {
        public void handleMessage(Message msg) {
            int type = msg.what;
            switch (type) {
                case BaiduNaviManager.TTSPlayMsgType.PLAY_START_MSG: {
                    Log.d(TAG, "Handler : TTS play start");
                    break;
                }
                case BaiduNaviManager.TTSPlayMsgType.PLAY_END_MSG: {
                    Log.d(TAG, "Handler : TTS play end");
                    break;
                }
                default:
                    showToastMsg("TTS验证失败");
                    Log.d(TAG, "TTS验证失败");
                    break;
            }
        }
    };

    private void initSetting() {
        // BNaviSettingManager.setDayNightMode(BNaviSettingManager.DayNightMode.DAY_NIGHT_MODE_DAY);
        BNaviSettingManager
                .setShowTotalRoadConditionBar(BNaviSettingManager.PreViewRoadCondition.ROAD_CONDITION_BAR_SHOW_ON);
        BNaviSettingManager.setVoiceMode(BNaviSettingManager.VoiceMode.Veteran);
        // BNaviSettingManager.setPowerSaveMode(BNaviSettingManager.PowerSaveMode.DISABLE_MODE);
        BNaviSettingManager.setRealRoadCondition(BNaviSettingManager.RealRoadCondition.NAVI_ITS_ON);
        Bundle bundle = new Bundle();
        // 必须设置APPID，否则会静音
        bundle.putString(BNCommonSettingParam.TTS_APP_ID, "0F5B74755E163F0570459A939D1DF18E");
        Log.d(TAG, "设置APPID");
        BNaviSettingManager.setNaviSdkParam(bundle);
    }

    /**
     * 内部TTS播报状态回调接口
     */
    private BaiduNaviManager.TTSPlayStateListener ttsPlayStateListener = new BaiduNaviManager.TTSPlayStateListener() {

        @Override
        public void playEnd() {
            Log.d(TAG, "TTSPlayStateListener : TTS play end");
        }

        @Override
        public void playStart() {
            Log.d(TAG, "TTSPlayStateListener : TTS play start");
        }
    };

    public void showToastMsg(final String msg) {
        Main2Activity.this.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Toast.makeText(Main2Activity.this, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    //初始化文件路径
    private boolean initDirs() {
        mSDCardPath = getSdcardDir();
        if (mSDCardPath == null) {
            return false;
        }
        File f = new File(mSDCardPath, APP_FOLDER_NAME);
        if (!f.exists()) {
            try {
                f.mkdir();
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    //判断是否有Sd卡
    private String getSdcardDir() {
        if (Environment.getExternalStorageState().equalsIgnoreCase(Environment.MEDIA_MOUNTED)) {
            return Environment.getExternalStorageDirectory().toString();
        }
        return null;
    }


    private boolean hasBasePhoneAuth() {
        if (Build.VERSION.SDK_INT >= 23) {
            PackageManager pm = this.getPackageManager();
            for (String auth : authComArr) {
                if (pm.checkPermission(auth, this.getPackageName()) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


    private void routeplanToNavi(LatLng currentLatLng, boolean isSimulation) {
        BNRoutePlanNode.CoordinateType mCoordinateType = BNRoutePlanNode.CoordinateType.BD09LL;
        if (!hasInitSuccess) {
            Toast.makeText(Main2Activity.this, "还未初始化!", Toast.LENGTH_SHORT).show();
        }
        // 权限申请
        if (Build.VERSION.SDK_INT >= 23) {
            // 保证导航功能完备
            if (!hasCompletePhoneAuth()) {
                if (!hasRequestComAuth) {
                    hasRequestComAuth = true;
                    this.requestPermissions(authComArr, authComRequestCode);
                    return;
                } else {
                    Toast.makeText(Main2Activity.this, "没有完备的权限!", Toast.LENGTH_SHORT).show();
                }
            }

        }

        BNRoutePlanNode sNode = null;
        BNRoutePlanNode eNode = null;


//        sNode = new BNRoutePlanNode(113.9512500000, 22.5489710000, "出发地", null, mCoordinateType);
//        eNode = new BNRoutePlanNode(113.9410840000, 22.5460020000, "终点站", null, mCoordinateType);

        sNode = new BNRoutePlanNode(mLongitude, mLatitude, "起始地", null, mCoordinateType);
        eNode = new BNRoutePlanNode(currentLatLng.longitude, currentLatLng.latitude, "目的地", null, mCoordinateType);

        if (sNode != null && eNode != null) {
            List<BNRoutePlanNode> list = new ArrayList<BNRoutePlanNode>();
            list.add(sNode);
            list.add(eNode);
            BaiduNaviManager.getInstance().launchNavigator(this, list, 1, isSimulation, new DemoRoutePlanListener(sNode));
        }
    }

    @Override
    public void onGetWalkingRouteResult(WalkingRouteResult walkingRouteResult) {
        Log.v("Amence", "ShowRoadWay===onGetWalkingRouteResult" + walkingRouteResult);


    }

    @Override
    public void onGetTransitRouteResult(TransitRouteResult transitRouteResult) {
        Log.v("Amence", "ShowRoadWay===onGetTransitRouteResult" + transitRouteResult);

    }

    @Override
    public void onGetMassTransitRouteResult(MassTransitRouteResult massTransitRouteResult) {
        Log.v("Amence", "ShowRoadWay===onGetMassTransitRouteResult" + massTransitRouteResult);

    }

    @Override
    public void onGetDrivingRouteResult(DrivingRouteResult result) {
        Log.v("Amence", "ShowRoadWay===onGetDrivingRouteResult====" + result);

        if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) {
            Toast.makeText(Main2Activity.this, "抱歉，未找到结果", Toast.LENGTH_SHORT).show();
        }
        if (result.error == SearchResult.ERRORNO.AMBIGUOUS_ROURE_ADDR) {
            // 起终点或途经点地址有岐义，通过以下接口获取建议查询信息
            // result.getSuggestAddrInfo()
            return;
        }
        if (result.error == SearchResult.ERRORNO.NO_ERROR) {
            nodeIndex = -1;


            if (result.getRouteLines().size() > 1) {
                nowResultdrive = result;
                route = result.getRouteLines().get(0);
                DrivingRouteOverlay overlay = new MyDrivingRouteOverlay(mBaiduMap);
                routeOverlay = overlay;
                mBaiduMap.setOnMarkerClickListener(overlay);
                overlay.setData(result.getRouteLines().get(0));
                overlay.addToMap();
                overlay.zoomToSpan();
            } else {
                Log.d("route result", "结果数<0");
                return;
            }

        }

    }

    @Override
    public void onGetIndoorRouteResult(IndoorRouteResult indoorRouteResult) {
        Log.v("Amence", "ShowRoadWay===onGetIndoorRouteResult" + indoorRouteResult);

    }

    @Override
    public void onGetBikingRouteResult(BikingRouteResult bikingRouteResult) {
        Log.v("Amence", "ShowRoadWay===onGetBikingRouteResult" + bikingRouteResult);

    }


    public class DemoRoutePlanListener implements BaiduNaviManager.RoutePlanListener {

        private BNRoutePlanNode mBNRoutePlanNode = null;

        public DemoRoutePlanListener(BNRoutePlanNode node) {
            mBNRoutePlanNode = node;
        }

        @Override
        public void onJumpToNavigator() {

            //设置途径点以及resetEndNode会回调该接口

            for (Activity ac : activityList) {
                if (ac.getClass().getName().endsWith("BNGuideActivity")) {
                    return;
                }
            }
            Intent intent = new Intent(Main2Activity.this, BNGuideActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(ROUTE_PLAN_NODE, (BNRoutePlanNode) mBNRoutePlanNode);
            intent.putExtras(bundle);
            startActivity(intent);

        }

        @Override
        public void onRoutePlanFailed() {
            // TODO Auto-generated method stub
            Toast.makeText(Main2Activity.this, "算路失败", Toast.LENGTH_SHORT).show();
        }
    }


    private boolean hasCompletePhoneAuth() {
        // TODO Auto-generated method stub

        PackageManager pm = this.getPackageManager();
        for (String auth : authComArr) {
            if (pm.checkPermission(auth, this.getPackageName()) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    /*****************************************************************************************/
    private void initMarker(User user) {


        if (user.getUserType() == 1) {//如果是乘客就获取司机
            scheduleForUserPull = Executors.newSingleThreadScheduledExecutor();
            final BitmapDescriptor bitmap = BitmapDescriptorFactory
                    .fromResource(R.drawable.car_64);
            scheduleForUserPull.scheduleWithFixedDelay(new Runnable() {
                public void run() {
                    try {
                        String driversJson = Util.getString(Main2Activity.this, Constant.SP_DRIVER_KEY);
                        List<User> customers = JSON.parseArray(driversJson, User.class);

                        buildMarker(customers, bitmap);
                    } catch (Throwable e) {
                        Log.v("Amence", "scheduleForUserPull error" + e.getMessage());
                    }

                }
            }, Constant.DEFAULT_SCHEDULE_INITIAL_DELAY, Constant.DEFAULT_SCHEDULE_LOCATION_DELAY, TimeUnit.SECONDS);


        } else {
            final BitmapDescriptor bitmap = BitmapDescriptorFactory
                    .fromResource(R.drawable.passenger_orange);
            scheduleForUserPull = Executors.newSingleThreadScheduledExecutor();
            scheduleForUserPull.scheduleWithFixedDelay(new Runnable() {
                public void run() {
                    try {
                        String driversJson = Util.getString(Main2Activity.this, Constant.SP_PASSENGER_KEY);
                        List<User> customers = JSON.parseArray(driversJson, User.class);
                        buildMarker(customers, bitmap);
                    } catch (Throwable e) {
                        Log.v("Amence", "scheduleForUserPull error" + e.getMessage());
                    }

                }
            }, Constant.DEFAULT_SCHEDULE_INITIAL_DELAY, Constant.DEFAULT_SCHEDULE_LOCATION_DELAY, TimeUnit.SECONDS);
        }


        /**司机获取乘客关系**/
        scheduleForPassengerPull = Executors.newSingleThreadScheduledExecutor();
        final BitmapDescriptor driverGetPassengerUserBitmap = BitmapDescriptorFactory.fromResource(R.drawable.passenger_orange);
        scheduleForPassengerPull.scheduleWithFixedDelay(new Runnable() {
            public void run() {
                try {
                    String driverGetPassengerJson = Util.getString(Main2Activity.this, Constant.SP_DRIVER_GET_PASSENGER_KEY);
                    if (driverGetPassengerJson != null) {
                        User driverGetPassengerUser = JSON.parseObject(driverGetPassengerJson, User.class);
                        List<User> listUser = new ArrayList<User>();
                        listUser.add(driverGetPassengerUser);
                        if (scheduleForUserPull != null && !scheduleForUserPull.isShutdown()) {
                            scheduleForUserPull.shutdown();
                        }
                        buildMarker(listUser, driverGetPassengerUserBitmap);
                    }
                } catch (Throwable e) {
                    Log.v("Amence", "scheduleForUserPull error" + e.getMessage());
                }

            }
        }, Constant.DEFAULT_SCHEDULE_INITIAL_DELAY, 10, TimeUnit.SECONDS);


        //存入目的司机数据
        scheduleForGetDriver = Executors.newSingleThreadScheduledExecutor();
        scheduleForGetDriver.scheduleWithFixedDelay(new Runnable() {
            public void run() {
                try {
                    if (aimDriver.getUserName() != null) {
                        Retrofit retrofit = RetrofitUtil.retrofitRequest();

                        UserService userService = retrofit.create(UserService.class);
                        //创建请求体
                        User user = new User();
                        user.setUserName(aimDriver.getUserName());

                        Call<BaseResponse> call = userService.getUser(user);

                        call.enqueue(new Callback<BaseResponse>() {
                            @Override
                            public void onResponse(Response<BaseResponse> response, Retrofit retrofit) {

                                BaseResponse baseResponse = response.body();

                                if (baseResponse != null && baseResponse.getMeta().isResult()) {
                                    String aimDriverJson = JSON.toJSONString(baseResponse.getData());
                                    Util.saveString(Main2Activity.this, Constant.SP_AIM_DRIVER_KEY, aimDriverJson);
                                }

                            }

                            @Override
                            public void onFailure(Throwable t) {
                                String message = t.getMessage();

                            }

                        });
                    }

                } catch (Throwable e) {
                    Log.v("Amence", "scheduleForUserPull error" + e.getMessage());
                }

            }
        }, Constant.DEFAULT_SCHEDULE_INITIAL_DELAY, 10, TimeUnit.SECONDS);


    }

    private void initView() {

        shwoAction = new TranslateAnimation(Animation.RELATIVE_TO_SELF, -1.0f,
                Animation.RELATIVE_TO_SELF, 0f, Animation.RELATIVE_TO_SELF,
                0.0f, Animation.RELATIVE_TO_SELF, 0.0f);
        shwoAction.setDuration(300);

        hideAction = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF,
                0.0f, Animation.RELATIVE_TO_SELF, 0.0f);
        hideAction.setDuration(300);


        mBaiduMap = mMapView.getMap();
        //隐藏缩放控件
        mMapView.showZoomControls(false);
        //是否显示比例尺，默认true
        mMapView.showScaleControl(true);
        zoom.setMapView(mMapView);
        //隐藏logo
        View child = mMapView.getChildAt(1);
        if (child != null && (child instanceof ImageView || child instanceof ZoomControls))
            child.setVisibility(View.GONE);

        //覆盖物的点击事件
        mBaiduMap.setOnMarkerClickListener(this);
        //地图的点击事件
        mBaiduMap.setOnMapClickListener(this);
        //地图状态事件
        mBaiduMap.setOnMapStatusChangeListener(this);
        mBaiduMap.setMyLocationEnabled(true);//开启定位图层
        MapStatusUpdate status = MapStatusUpdateFactory.zoomTo(15.0F);
        mBaiduMap.setMapStatus(status);

    }


    //定位
    private void initLocation() {


        mSearch = RoutePlanSearch.newInstance();
        mSearch.setOnGetRoutePlanResultListener((OnGetRoutePlanResultListener) this);

        mLocationClient = new LocationClient(getApplicationContext());
        myLocationListener = new MyLoacationListener();
        mLocationClient.registerLocationListener(myLocationListener);

        LocationClientOption options = new LocationClientOption();
        options.setCoorType("bd09ll");//坐标类型
        options.setIsNeedAddress(true);
        options.setOpenGps(true);
        options.setScanSpan(1000);

        mLocationClient.setLocOption(options);

        //模式
        mLocationMode = MyLocationConfiguration.LocationMode.NORMAL;

        mLoacationBitmap = BitmapDescriptorFactory.fromResource(R.mipmap.navi_map_gps_locked);

        myOrentationListener = new MyOrentationListener(getApplicationContext());
        myOrentationListener.setmListener(new MyOrentationListener.OnOrientationListner() {
            @Override
            public void onOrientationChanged(float x) {
                mCurrentX = x;
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_1: //普通地图
                mBaiduMap.setMapType(BaiduMap.MAP_TYPE_NORMAL);
                break;
            case R.id.item_2://卫星地图
                mBaiduMap.setMapType(BaiduMap.MAP_TYPE_SATELLITE);
                break;
            case R.id.item_3:
                if (mBaiduMap.isTrafficEnabled()) {
                    mBaiduMap.setTrafficEnabled(false);
                    item.setTitle("实时交通ON");
                } else {
                    mBaiduMap.setTrafficEnabled(true);
                    item.setTitle("实时交通OFF");
                }
                break;
            case R.id.item_4:
                mLocationMode = MyLocationConfiguration.LocationMode.NORMAL;
                break;
            case R.id.item_5://跟随模式
                mLocationMode = MyLocationConfiguration.LocationMode.FOLLOWING;
                break;
            case R.id.item_6://罗盘模式
                mLocationMode = MyLocationConfiguration.LocationMode.COMPASS;
                break;
            case R.id.item_7:
//                addOverLays(MapCar.infos);
                break;
            case R.id.item_8:
                Toast.makeText(this, currentLatLng.latitude + "\n" + currentLatLng.longitude, Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void buildMarker(List<User> users, BitmapDescriptor bitmap) {
        //首先清楚定位的浮层
        mBaiduMap.clear();
        LatLng latLng = null;
        Marker marker = null;
        OverlayOptions options;
        for (User user : users) {
            //经纬度
            latLng = new LatLng(user.getLatitude(), user.getLongitude());
            //图标
            options = new MarkerOptions().position(latLng).icon(bitmap).zIndex(5);

            marker = (Marker) mBaiduMap.addOverlay(options);
            LatLng position = marker.getPosition();
            Util.saveString(Main2Activity.this,Constant.SP_LOCAL_KEY,position.latitude+","+position.longitude);
            Bundle bundle = new Bundle();
            bundle.putParcelable("user", user);
            marker.setExtraInfo(bundle);
        }

        //定位到覆盖物的位置，应该是最后一个覆盖物的位置，如果不想定位到覆盖物的位置而是定位到人的位置，注释掉
//        MapStatusUpdate msu = MapStatusUpdateFactory.newLatLng(latLng);
//        mBaiduMap.setMapStatus(msu);
    }

    @OnClick({R.id.dh, R.id.mn, R.id.makeSureBut, R.id.showRoad, R.id.finishBut, R.id.aliPayBut, R.id.weChatPayBut, R.id.payLinearLayout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.dh:
                routeplanToNavi(currentLatLng, true);
                break;
            case R.id.mn:
                routeplanToNavi(currentLatLng, false);
                break;
            case R.id.showRoad:

//                showRoadWay();
                break;
            case R.id.makeSureBut:
                makeSureBut.setVisibility(View.GONE);
                finishBut.setVisibility(View.VISIBLE);
                break;
            case R.id.finishBut:
                finishOrder();
                payLinearLayout.setVisibility(View.VISIBLE);
                break;

            case R.id.aliPayBut: //支付宝支付
                Util.startAliPay(Main2Activity.this);
                payLinearLayout.setVisibility(View.GONE);
                break;

            case R.id.weChatPayBut: //微信支付
                Util.startWechat(Main2Activity.this);
                payLinearLayout.setVisibility(View.GONE);
                break;

            default:
                break;
        }

    }

    //展示路径规划
    private void showRoadWay() {
        scheduleForShowRoad = Executors.newSingleThreadScheduledExecutor();
        final BitmapDescriptor bitmap = BitmapDescriptorFactory
                .fromResource(R.drawable.car_64);
        scheduleForShowRoad.scheduleWithFixedDelay(new Runnable() {
            public void run() {
                try {

                    String aimDriverJson = Util.getString(Main2Activity.this, Constant.SP_AIM_DRIVER_KEY);
                    if (aimDriverJson != null) {
                        User aimDriver = JSON.parseObject(aimDriverJson, User.class);
                        List<User> list = new ArrayList<User>();
                        list.add(aimDriver);
                        buildMarker(list, bitmap);

                        PlanNode stMassNode = PlanNode.withLocation(new LatLng(aimDriver.getLatitude(), aimDriver.getLongitude()));
                        PlanNode enMassNode = PlanNode.withLocation(new LatLng(user.getLatitude(), user.getLongitude()));
                        mSearch.drivingSearch((new DrivingRoutePlanOption()).from(stMassNode).to(enMassNode));
                        showRoadBut.setVisibility(View.GONE);
                    }


                } catch (Throwable e) {
                    Log.v("Amence", "scheduleForShowRoad error" + e.getMessage());
                }

            }
        }, Constant.DEFAULT_SCHEDULE_INITIAL_DELAY, 10, TimeUnit.SECONDS);


    }

    //司机点击完成行程
    private void finishOrder() {

        Retrofit retrofit = RetrofitUtil.retrofitRequest();

        LinkService linkService = retrofit.create(LinkService.class);
        PassengerDriverLink link;
        if (user.getUserType() == 1) {
            link = new PassengerDriverLink(user.getUserName(), null);

        } else {
            link = new PassengerDriverLink(null, user.getUserName());

        }
        Call<BaseResponse> call = linkService.delete(link);

        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Response<BaseResponse> response, Retrofit retrofit) {

                BaseResponse baseResponse = response.body();

                if (baseResponse != null && baseResponse.getMeta().isResult()) {
                    //此时显示所有的乘客
                    initMarker(user);
                    finishBut.setVisibility(View.GONE);
                    Util.saveString(Main2Activity.this, Constant.SP_DRIVER_GET_PASSENGER_KEY, null);

                } else {
                    String message = baseResponse.getMeta().getMsg();
                    Log.v("Amence", "login error " + message);

                }
            }

            @Override
            public void onFailure(Throwable t) {
                String message = t.getMessage();

            }

        });


    }

    //覆盖物的点击事件
    @Override
    public boolean onMarkerClick(Marker marker) {
        Bundle bundle = marker.getExtraInfo();
        final User customer = (User) bundle.getParcelable("user");

        TextView infoText = new TextView(getApplicationContext());
        infoText.setBackgroundResource(R.mipmap.location_tips);
        infoText.setPadding(30, 20, 30, 30);
        infoText.setText(customer.getUserName());
        infoText.setTextColor(Color.parseColor("#FFFFFF"));

        mBitmapWindow = BitmapDescriptorFactory.fromView(infoText);
        final LatLng lng = marker.getPosition();
        Point p = mBaiduMap.getProjection().toScreenLocation(lng);
        p.y -= 47;
        currentLatLng = mBaiduMap.getProjection().fromScreenLocation(p);

        infoWindow = new InfoWindow(mBitmapWindow, currentLatLng, 0, new InfoWindow.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick() {
                mBaiduMap.hideInfoWindow();
                stopAnimation();
            }
        });
        mBaiduMap.showInfoWindow(infoWindow);
        //当乘客点击出租车时，callDriverBut显示。
        if (user.getUserType() == 1) {
            callDriverBut.setVisibility(View.VISIBLE);

            // 乘客点击车辆，出现 呼叫此车，地图只剩下这辆车，将司机乘客关系插入表中
            callDriverBut.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Retrofit retrofit = RetrofitUtil.retrofitRequest();

                    LinkService linkService = retrofit.create(LinkService.class);
                    //创建请求体
                    final PassengerDriverLink link = new PassengerDriverLink(user.getUserName(), customer.getUserName());

                    final Call<BaseResponse> call = linkService.add(link);

                    call.enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Response<BaseResponse> response, Retrofit retrofit) {

                            BaseResponse body = response.body();
                            if (body != null && body.getMeta().isResult()) {
                                final BitmapDescriptor bitmap;
                                if (user.getUserType() == 1) {
                                    bitmap = BitmapDescriptorFactory
                                            .fromResource(R.drawable.car_64);
                                } else {
                                    bitmap = BitmapDescriptorFactory
                                            .fromResource(R.drawable.passenger_orange);
                                }
                                if (scheduleForUserPull != null) {
                                    scheduleForUserPull.shutdown();
                                    List<User> list = new ArrayList<User>();
                                    list.add(customer);
                                    buildMarker(list, bitmap);
                                    callDriverBut.setVisibility(View.GONE);
                                    finishBut.setVisibility(View.VISIBLE);
                                    //这只终点坐标
                                    mEndLatitude = lng.latitude;
                                    mEndLongitude = lng.longitude;
                                    mEndLatLng = lng;
                                    aimDriver = customer;

                                }

                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {

                        }

                    });
                }
            });
        } else {   //当出租车点击用户时，callDriverBut显示。

            makeSureBut.setVisibility(View.VISIBLE);
            startAnimation();
        }


        return true;
    }

    private void startAnimation() {
        if (dh.getVisibility() == View.VISIBLE || mn.getVisibility() == View.VISIBLE)
            return;
        dh.startAnimation(shwoAction);
        mn.startAnimation(shwoAction);
        dh.setVisibility(View.VISIBLE);
        mn.setVisibility(View.VISIBLE);
    }

    private void stopAnimation() {
        if (dh.getVisibility() == View.GONE || mn.getVisibility() == View.GONE)
            return;
        dh.startAnimation(hideAction);
        mn.startAnimation(hideAction);
        dh.setVisibility(View.GONE);
        mn.setVisibility(View.GONE);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        mBaiduMap.hideInfoWindow();
        stopAnimation();
    }

    @Override
    public boolean onMapPoiClick(MapPoi mapPoi) {
        return false;
    }


    /**
     * 手势操作地图
     *
     * @param mapStatus
     */
    @Override
    public void onMapStatusChangeStart(MapStatus mapStatus) {

    }

    @Override
    public void onMapStatusChangeStart(MapStatus mapStatus, int i) {

    }


    /**
     * 地图变化中
     *
     * @param mapStatus
     */
    @Override
    public void onMapStatusChange(MapStatus mapStatus) {
        mBaiduMap.hideInfoWindow();
        stopAnimation();
    }

    /**
     * 地图状态改变
     *
     * @param mapStatus
     */
    @Override
    public void onMapStatusChangeFinish(MapStatus mapStatus) {

    }


    private class MyLoacationListener implements BDLocationListener {

        @Override
        public void onReceiveLocation(BDLocation bdLocation) {
            MyLocationData data = new MyLocationData.Builder()
                    .direction(mCurrentX)//方向
                    .accuracy(bdLocation.getRadius())
                    .latitude(bdLocation.getLatitude())
                    .longitude(bdLocation.getLongitude())
                    .build();

            mBaiduMap.setMyLocationData(data);

            //得到经纬度
            mLatitude = bdLocation.getLatitude();
            mLongitude = bdLocation.getLongitude();

            zoom.setLocation(mLatitude, mLongitude);

            /**
             * NORMAL:不会实时更新位置
             * COMPASS:实时更新位置,罗盘
             * FOLLOWING:跟随模式
             */
            MyLocationConfiguration config = new MyLocationConfiguration(mLocationMode, true, mLoacationBitmap);
            mBaiduMap.setMyLocationConfigeration(config);
//            mBaiduMap.setMyLocationEnabled(true);//当不需要定位图层时关闭定位图层

            if (isFirstIn) {
                LatLng latLbg = new LatLng(bdLocation.getLatitude(), bdLocation.getLongitude());//经纬度
                MapStatusUpdate msu = MapStatusUpdateFactory.newLatLng(latLbg);
                mBaiduMap.animateMapStatus(msu);
                isFirstIn = false;

                Toast.makeText(Main2Activity.this, bdLocation.getAddrStr(), Toast.LENGTH_SHORT).show();
            }
        }


    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus && Build.VERSION.SDK_INT > 19) {
            getWindow().getDecorView()
                    .setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //开启定位
        mBaiduMap.setMyLocationEnabled(true);
        if (!mLocationClient.isStarted())
            mLocationClient.start();
        //开启方向传感器
        myOrentationListener.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    protected void onResume() {
        super.onStop();
        mMapView.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //结束定位
        mBaiduMap.setMyLocationEnabled(false);
        mLocationClient.stop();
        //关闭方向传感器
        myOrentationListener.stop();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        // TODO Auto-generated method stub
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == authBaseRequestCode) {
            for (int ret : grantResults) {
                if (ret == 0) {
                    continue;
                } else {
                    Toast.makeText(Main2Activity.this, "缺少导航基本的权限!", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
            initNavigatin();
        } else if (requestCode == authComRequestCode) {
            for (int ret : grantResults) {
                if (ret == 0) {
                    continue;
                }
            }
            routeplanToNavi(currentLatLng, true);
        }

    }

    boolean useDefaultIcon = false;

    private class MyDrivingRouteOverlay extends DrivingRouteOverlay {

        public MyDrivingRouteOverlay(BaiduMap baiduMap) {
            super(baiduMap);
        }

        @Override
        public BitmapDescriptor getStartMarker() {
            if (useDefaultIcon) {
                return BitmapDescriptorFactory.fromResource(R.drawable.icon_st);
            }
            return null;
        }

        @Override
        public BitmapDescriptor getTerminalMarker() {
            if (useDefaultIcon) {
                return BitmapDescriptorFactory.fromResource(R.drawable.icon_en);
            }
            return null;
        }
    }


}
