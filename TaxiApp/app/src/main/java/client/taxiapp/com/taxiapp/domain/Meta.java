package client.taxiapp.com.taxiapp.domain;

import java.io.Serializable;


/**
 * Created by Amence on 2018/4/28.
 */

public class Meta implements Serializable {

      private boolean           result;
      private Integer           code;
      private String            msg;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
