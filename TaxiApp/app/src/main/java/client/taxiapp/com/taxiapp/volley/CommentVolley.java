package client.taxiapp.com.taxiapp.volley;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import client.taxiapp.com.taxiapp.util.Util;

/**
 * Created by Amence on 2016/11/10.
 */

public class CommentVolley<T> {

    public static boolean isCache = false;
    public static String cacheKey;
    private Context mContext;

    T jsonData;

    /**
     * volley 获取获取网络数据通用方法
     *
     * @param url
     * @param context
     * @param t
     * @return
     */
    public void getDateByVolley(String url, Context context, JSONObject jsonRequest,final T t, final VolleyCallBack callBack) {
        this.mContext = context;
        RequestQueue queue = Volley.newRequestQueue(context);

        JsonObjectRequest json = new JsonObjectRequest(Request.Method.POST,url, jsonRequest , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                setCacheString(jsonObject.toString());
                jsonData = Util.parseGson(jsonObject.toString(), t);
                if (callBack != null) {
                    callBack.success(jsonData);

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        queue.add(json);

    }

    public interface VolleyCallBack<T> {
        void success(T t);

        void error(String str);

    }

    public void setCacheString(String json) {
        Util.saveString(mContext, this.cacheKey, json);
    }


}
