package client.taxiapp.com.taxiapp.util;

import java.util.concurrent.TimeUnit;

/**
 * Created by Amence on 2017/1/18.
 */

public class Constant {

    public static final int                     DEFAULT_PAGE_SIZE                           = 10;
    public static final int                     DEFAULT_SCHEDULE_INITIAL_DELAY              = 5;
    public static final int                     DEFAULT_SCHEDULE_DELAY                      = 20;
    public static final int                     DEFAULT_SCHEDULE_LOCATION_DELAY             = 25;
    public static final TimeUnit                DEFAULT_SCHEDULE_TIME_UNIT                  = TimeUnit.SECONDS;

    public static final String                  LOGIN                                       = "0";
    public static final String                  SP_USER_KEY                                 = "SP_USER_KEY" ;
    public static final String                  SP_AIM_DRIVER_KEY                           = "SP_AIM_DRIVER_KEY" ;
    public static final String                  SP_DRIVER_KEY                               = "SP_DRIVER_KEY" ;
    public static final String                  SP_LOCAL_KEY                                = "SP_LOCAL_KEY" ;
    public static final String                  SP_LINK_KEY                                 = "SP_LINK_KEY" ;
    public static final String                  SP_PASSENGER_KEY                            = "SP_PASSENGER_KEY" ;
    public static final String                  SP_DRIVER_GET_PASSENGER_KEY                 = "SP_DRIVER_GET_PASSENGER_KEY" ;

    private  static String version = "1.0";


}
