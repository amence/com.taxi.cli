package client.taxiapp.com.taxiapp.application;

import android.app.Application;

import com.baidu.mapapi.SDKInitializer;

/**
 * Created by Amence on 2018/5/1.
 */

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        SDKInitializer.initialize(this);
    }
}
