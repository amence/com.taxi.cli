package client.taxiapp.com.taxiapp.domain;

import java.io.Serializable;

/**
 * Created by Amence on 2018/4/28.
 */

public class BaseResponse implements Serializable {

    private Meta          meta;
    private Object        data;


    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
