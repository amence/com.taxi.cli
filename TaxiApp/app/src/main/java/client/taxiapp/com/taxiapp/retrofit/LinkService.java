package client.taxiapp.com.taxiapp.retrofit;

import client.taxiapp.com.taxiapp.domain.BaseResponse;
import client.taxiapp.com.taxiapp.domain.PassengerDriverLink;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;

/**
 * Created by Amence on 2018/5/1.
 */

public interface LinkService {

    //选择司机
    @Headers({"Content-Type: application/json","Accept: application/json"})//需要添加头
    @POST("/link/add")
    Call<BaseResponse> add(@Body PassengerDriverLink link);



    //行程完成之后删除关系
    @Headers({"Content-Type: application/json","Accept: application/json"})//需要添加头
    @POST("/link/delete")
    Call<BaseResponse> delete(@Body PassengerDriverLink link);


    //行程完成之后删除关系
    @Headers({"Content-Type: application/json","Accept: application/json"})//需要添加头
    @POST("/link/getPassenger")
    Call<BaseResponse> getPassenger(@Body PassengerDriverLink link);
}
