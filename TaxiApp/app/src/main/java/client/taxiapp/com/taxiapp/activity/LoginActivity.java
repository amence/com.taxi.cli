package client.taxiapp.com.taxiapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;

import java.util.List;

import client.taxiapp.com.taxiapp.R;
import client.taxiapp.com.taxiapp.customer.ClearEditText;
import client.taxiapp.com.taxiapp.customer.util.Utils;
import client.taxiapp.com.taxiapp.domain.BaseResponse;
import client.taxiapp.com.taxiapp.domain.User;
import client.taxiapp.com.taxiapp.retrofit.UserService;
import client.taxiapp.com.taxiapp.service.DriverGetPassengerServer;
import client.taxiapp.com.taxiapp.service.DriverServer;
import client.taxiapp.com.taxiapp.service.LocalServer;
import client.taxiapp.com.taxiapp.service.PassengerServer;
import client.taxiapp.com.taxiapp.util.Constant;
import client.taxiapp.com.taxiapp.util.RetrofitUtil;
import client.taxiapp.com.taxiapp.util.StackManager;
import client.taxiapp.com.taxiapp.util.Util;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Amence on 2018/4/28.
 */

public class LoginActivity extends Activity {

    private Button mLoginBut;

    private ClearEditText mPhone;

    private ClearEditText mPwd;

    private TextView mRegisterText;

    private TextView mForgetPwd;

    private TextView mJumpTxt;

    private ImageView mBackImg;

    private String pwdStr;

    private String name;

    boolean isSuccess = false;

    private RequestQueue mRequestQueue;

    private StringRequest stringRequest;

    private boolean isCheck = false;
    String userJson;

    private final static int SHOW_RESPONSE = 1;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        StackManager.setCurrentActivity(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            isCheck = extras.getBoolean("isCheck");
        }


        String login = Util.getSharePreference(this).getString(Constant.LOGIN, "0");
//        if(login.equals("1")){
//            startActivity(new Intent(this,FlashActivity.class));
//            finish();
//        }
        initLayout();
        doListener();
    }


    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SHOW_RESPONSE:
                    Toast.makeText(LoginActivity.this, msg.obj+"", Toast.LENGTH_SHORT).show();

                    break;
            }
        }
    };

    private void doListener() {


        if (isCheck) {
            mLoginBut.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(LoginActivity.this, "您已经登陆", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            mLoginBut.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    String phone = mPhone.getText().toString().trim();
                    if (TextUtils.isEmpty(phone)) {
                        Toast.makeText(LoginActivity.this, "请输入邮箱", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    String pwd = mPwd.getText().toString().trim();
                    if (TextUtils.isEmpty(pwd)) {
                        Toast.makeText(LoginActivity.this, "请输入密码", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    try {
                        getStatusFromNet();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }


        mForgetPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, ForgetPwdActivity.class));
            }
        });


        mRegisterText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));

            }
        });


        mJumpTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                finish();

            }
        });

        mBackImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }

    private void initLayout() {
        mLoginBut = (Button) findViewById(R.id.btn_login);
        mPhone = (ClearEditText) findViewById(R.id.etxt_phone);
        mPwd = (ClearEditText) findViewById(R.id.etxt_pwd);
        mRegisterText = (TextView) findViewById(R.id.txt_toReg);
        mForgetPwd = (TextView) findViewById(R.id.register_text);
        mJumpTxt = (TextView) findViewById(R.id.login_jump);
        mBackImg = (ImageView) findViewById(R.id.login_back_img);

        if (isCheck) {
            mPhone.setText(Util.getString(this, "phoneStr"));
            mPwd.setText(Util.getString(this, "pwdStr"));
            mJumpTxt.setVisibility(View.GONE);
            mBackImg.setVisibility(View.VISIBLE);
            mRegisterText.setVisibility(View.GONE);
            mForgetPwd.setVisibility(View.GONE);
        } else {
            mJumpTxt.setVisibility(View.VISIBLE);
            mLoginBut.setClickable(true);
            mBackImg.setVisibility(View.GONE);
        }

    }

    public void getStatusFromNet() throws JSONException {

        name = mPhone.getText().toString();
        pwdStr = mPwd.getText().toString();


        Retrofit retrofit = RetrofitUtil.retrofitRequest();

        UserService userService = retrofit.create(UserService.class);
        //创建请求体
        User user = new User(name, pwdStr);

        Call<BaseResponse> call = userService.login(user);

        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Response<BaseResponse> response, Retrofit retrofit) {

                BaseResponse baseResponse = response.body();

                if (baseResponse.getMeta().isResult()) {
                    userJson = JSON.toJSONString(baseResponse.getData());
                    Util.saveString(LoginActivity.this, Constant.SP_USER_KEY, userJson);
                    User vo = JSON.parseObject(userJson, User.class);

                    if(vo.getUserType()==1){
                        startService(new Intent(LoginActivity.this, DriverServer.class));
                    }else{
                        startService(new Intent(LoginActivity.this, PassengerServer.class));
                        startService(new Intent(LoginActivity.this, DriverGetPassengerServer.class));
                    }

                    startService(new Intent(LoginActivity.this, LocalServer.class));
//                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    startActivity(new Intent(LoginActivity.this, Main2Activity.class));
                    LoginActivity.this.finish();
                } else {
                    String data = baseResponse.getMeta().getMsg();
                    Message message = new Message();
                    message.what = SHOW_RESPONSE;
                    // 将服务器返回的结果存放到Message中
                    message.obj = data;
                    handler.sendMessage(message);

                }
            }

            @Override
            public void onFailure(Throwable t) {
                Message message = new Message();
                message.what = SHOW_RESPONSE;
                // 将服务器返回的结果存放到Message中
                message.obj = "请检查网络！";
                handler.sendMessage(message);

            }

        });


    }
}
