package client.taxiapp.com.taxiapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import client.taxiapp.com.taxiapp.R;
import client.taxiapp.com.taxiapp.customer.ClearEditText;
import client.taxiapp.com.taxiapp.customer.MyCheckBox;
import client.taxiapp.com.taxiapp.domain.BaseResponse;
import client.taxiapp.com.taxiapp.domain.User;
import client.taxiapp.com.taxiapp.retrofit.UserService;
import client.taxiapp.com.taxiapp.service.LocalServer;
import client.taxiapp.com.taxiapp.util.RetrofitUtil;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class RegisterActivity extends Activity {

    private ImageView mBackImg;

    private ClearEditText mPhoneTxt;

    private ClearEditText mPwdTxt;

    private ClearEditText mEmail;

    private Button mLoginBut;

    private MyCheckBox myCheckBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initLayout();
        doListener();


    }


    private void initLayout() {
        mBackImg = (ImageView) findViewById(R.id.register_back_img);
        mPhoneTxt = (ClearEditText) findViewById(R.id.register_edittxt_phone);
        mPwdTxt = (ClearEditText) findViewById(R.id.register_edittxt_pwd);
        mEmail = (ClearEditText) findViewById(R.id.register_edittxt_email);
        mLoginBut = (Button) findViewById(R.id.register_btn_login);
        myCheckBox = (MyCheckBox) findViewById(R.id.myCheckBox);

    }


    private void doListener() {

        mBackImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        mLoginBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = mPhoneTxt.getText().toString();
                String pwdStr = mPwdTxt.getText().toString();
                String email = mEmail.getText().toString();
                boolean checked = myCheckBox.isChecked();
                Retrofit retrofit = RetrofitUtil.retrofitRequest();

                UserService userService = retrofit.create(UserService.class);
                //创建请求体
                User user = new User(name, pwdStr, email);
                if (checked) {
                    user.setUserType(2);//司机
                }else{
                    user.setUserType(1);//乘客
                }
                Call<BaseResponse> call = userService.register(user);


                call.enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Response<BaseResponse> response, Retrofit retrofit) {

                        BaseResponse baseResponse = response.body();

                        if(baseResponse.getMeta().isResult()){
                            startActivity(new Intent(RegisterActivity.this, Main2Activity.class));
                            RegisterActivity.this.finish();
                            startService(new Intent(RegisterActivity.this,LocalServer.class));
                        }else{
                            String message =  baseResponse.getMeta().getMsg();

                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        String message = t.getMessage();

                    }

                });

            }
        });

    }
}
