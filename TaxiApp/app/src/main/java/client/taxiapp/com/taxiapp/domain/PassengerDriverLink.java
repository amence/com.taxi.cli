package client.taxiapp.com.taxiapp.domain;

import java.io.Serializable;

/**
 * Created by Amence on 2018/5/1.
 */

public class PassengerDriverLink implements Serializable {

    Integer             id;                      //id
    String              passengerName;
    String              driverName;


    public PassengerDriverLink() {
    }

    public PassengerDriverLink(String passengerName, String driverName) {
        this.passengerName = passengerName;
        this.driverName = driverName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }
}